# MySubscriptionsTableColumns

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_1_20131107
* License: MIT

## Overview
This component makes the My Subscriptions page use dDocName and dDocTitle columns.

* Dynamichtml Includes:
	- subscription_info_cells: Core - override to use dDocName and dDocTitle columns
	- subscription_list: Core - override to use dDocName and dDocTitle columns

* Templates:
	- SUBSCRIPTION_LISTS: Core - override to use subscription_list dynamichtml include

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 7.5.1Update (071231)

## Changelog
* build_1_20131107
	- Initial component release